#ifndef _CLASS_JUTALEK_
#define _CLASS_JUTALEK_

#include "main_header.h"
#include "structs.h"
#include "class_kvalifikacio.h"

class Jutalek // �r�k�thet�
{
	protected:
		string nev;
		Kvalifikacio *kcio;
		Melyseg melyseg; // 0 az a saj�t �s front fogyasztoi szintje
		bool vegtelenMely;
		LabAlapuElv labElv;
		float szazalek;
		bool pontAlapu; // pont vagy bev�tel alap� (ha pont, felszorozzuk pont�rt�kkel �s azt adjuk a bev�telhez)
		vector<Szint*> megengedettSzintek;
		bool engedSpillover;
	public:

		Jutalek(const char *nev, Kvalifikacio *kcio, const unsigned& mtol, const unsigned& mig, const float& szazalek, const bool& vegtelenMely = false, const bool& pontAlapu = false, const LabAlapuElv& labElv = NEM_LABALAPU, const bool& engedSpillover = true)
			: nev(nev), kcio(kcio), szazalek(szazalek), vegtelenMely(vegtelenMely), pontAlapu(pontAlapu), labElv(labElv), engedSpillover(engedSpillover)
		{
			melyseg.tol = mtol;
			melyseg.ig = mig;
		}

		// workerek
		const double& Kiszamol(Tag *tag);

		// getterek
		const string& getNev() const;
		Kvalifikacio *getKcio() const;
		const Melyseg& getMelyseg() const;
		const LabAlapuElv& getLabElv() const;
		const float& getSzazalek() const;
		Szint* getMegengedettSzint(const unsigned& index, bool *letezik) const;
		const bool& getVegtelenMely() const;

		// setterek
		void setNev(const string& set);
		void setKcio(Kvalifikacio *set);
		void setMelyseg(const Melyseg& set);
		void setLabElv(const LabAlapuElv& set);
		void setSzazalek(const float& set);
		void setMegengedettSzint(Szint *minta, const unsigned& index);

		// adderek
		void adMegengedettSzint(Szint *minta);
};

#endif