#ifndef _CLASS_JUTALEK_SZAMITO_
#define _CLASS_JUTALEK_SZAMITO_

#include "main_header.h"
#include "class_jutalek.h"

class JutalekSzamito // örökíthető
{
	protected:
		vector<Jutalek*> jutalekok;
		bool vegtelenGen;
		unsigned maxGen;
	public:

		JutalekSzamito() : vegtelenGen(false), maxGen(0) {}

		// workerek
		virtual void Szamitas(vector<Tag*>& levelek, JutalekSzamito& peldany); // nem kell tagok, levelektől indul
		virtual void Szamit(Tag *tag);
		void Jogosultsagok(Tag *tag);

		// getterek
		const Jutalek& getJutalek(const unsigned& index, bool *letezik) const;
		const bool& getVegtelenGen() const;
		const unsigned& getMaxGen() const;

		// setterek
		void setJutalek(const Jutalek& minta, const unsigned& index);
		void setVegtelenGen(const bool& set);
		void setKotHavi(const unsigned& set);

		// adder
		void adJutalek(Jutalek *jut);
};

#endif