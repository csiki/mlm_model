#include "stdafx.h"
#include "main_header.h"
#include "class_tagFelvetel.h"
#include "class_fogyaszto.h"
#include "class_tag.h"
#include "class_halozat.h"

///// worker /////
void TagFelvetel::NulladigFelvetel(vector<Tag*>& tagok)
{
	tagok.clear();
	for (unsigned i=0; i<nullTagletszam; ++i)
		tagok.push_back(new Tag(i,0,NULL,osszCsomag,NULL,alapszint,osszKcio));
}

void TagFelvetel::Felvetel(vector<Tag*>& tagok, const unsigned& idoszak, const Halozat *halozat)
{
	unsigned randSponsor, tagokMeret, behivni;
	tagokMeret = tagok.size();
	behivni = tagokMeret * atlagBehivott;
	
	for (unsigned t=0; t<behivni; ++t)
	{
		randSponsor = unsigned(rand() % tagok.size());
		tagok.push_back(new Tag(tagokMeret + t, idoszak, tagok[randSponsor], osszCsomag, NULL, alapszint, osszKcio));
		tagok[tagokMeret + t]->Vasarol(halozat->RandCsomag());
		tagok[randSponsor]->Felvetel(tagok[tagokMeret + t], halozat->getKompTerv(), halozat->getMaxKozv());
	}
}

void TagFelvetel::MasolCsomagok(vector<Csomag*>& csomagok)
{
	osszCsomag = csomagok;
}

void TagFelvetel::MasolKciok(vector<Kvalifikacio*>& kciok)
{
	osszKcio = kciok;
}

///// getter /////
const double& TagFelvetel::getAtlagBehivott() const
{
	return atlagBehivott;
}

///// setter /////
void TagFelvetel::setAtlagBehivott(const double& set)
{
	atlagBehivott = set;
}

///// adder /////
void TagFelvetel::adOsszCsomag(Csomag *cs)
{
	if (cs != NULL) osszCsomag.push_back(cs);
}