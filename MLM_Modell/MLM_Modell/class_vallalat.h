#ifndef _CLASS_VALLALAT_
#define _CLASS_VALLALAT_

#include "main_header.h"
#include "structs.h"

class Vallalat
{
	private:
		string nev;
		long double osszbev, osszki;
		Forgalom forgalom; // adott id�szak�
		Forgalom egyebForgalom;
		vector<Csomag*> csomagok;
	public:
		Vallalat(const char *nev, const double& egyebBe = 0, const double& egyebKi = 0) : nev(nev)
		{
			osszbev = osszki = 0;
			forgalom.bev = forgalom.ki = 0;
			egyebForgalom.bev = egyebBe;
			egyebForgalom.ki = egyebKi;
		}

		// workerek
		void ForgalomSzamitas(vector<Tag*>& tagok, vector<Fogyaszto*>& fogyasztok);
		void BiztositCsomagok(TagFelvetel *tagfelv, FogyasztoFelvetel *fogyfelv);

		// getterek
		const string& getNev() const;
		const long double& getOsszBev() const;
		const long double& getOsszKi() const;
		const Forgalom& getForgalom() const;
		const Forgalom& getEgyebForgalom() const;

		// adderek
		void adCsomag(Csomag *minta);

		// setterek
		void setNev(const char *set);
		void setCsomag(const Csomag& minta, const unsigned& index);
		void setVallalatEgyebForgalom(const Forgalom& minta);
};

#endif