#ifndef _CLASS_SZINT_
#define _CLASS_SZINT_

#include "main_header.h"

class Szint
{
	private:
		string nev;
		Kvalifikacio *kcio;
		unsigned rangsor; // 0 a legalsóbb szint, N>0 magasabb szint
	public:
		Szint (const char* nev, Kvalifikacio *kcio, const unsigned& rangsor = 0)
			: nev(nev), kcio(kcio), rangsor(rangsor)
		{}
		// getterek
		const string& getNev() const;
		Kvalifikacio *getKcio() const;
		const unsigned& getRangsor() const;

		// setterek
		void setNev(const char *set);
		void setKcio(Kvalifikacio *set);
		void setRangsor(const unsigned& set);
};

#endif