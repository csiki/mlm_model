#include "stdafx.h"
#include "main_header.h"
#include "class_halozat.h"
#include "class_tag.h"

Halozat::Halozat()
{
}

Halozat::~Halozat()
{
}

////// workerek //////
void Halozat::KciokKiosztasa(vector<Tag*>& tagok)
{
	for (unsigned t=0; t<tagok.size(); ++t)
		tagok[t]->KciokKiosztasa();
}

Csomag* Halozat::RandCsomag() const
{
	if (kotelezoCsomagok.size() > 0)
		return kotelezoCsomagok[rand() % kotelezoCsomagok.size()];
	else if (ajanlottCsomagok.size() > 0)
		return ajanlottCsomagok[rand() % ajanlottCsomagok.size()];
	
	return NULL;
}

void Halozat::BiztositKciok(TagFelvetel *tagfelv)
{
	tagfelv->MasolKciok(kciok);
}

void Halozat::PontSzamitonakIdoszakAtad(unsigned *i)
{
	pontSzamito->setIdoszak(i);
}

void Halozat::SzintekKiosztasa(vector<Tag*>& tagok)
{
	for (unsigned t=0; t<tagok.size(); ++t)
	{
		vector<Kvalifikacio*>& k = tagok[t]->accessKciok();
		for (unsigned sz=0; sz<szintek.size(); ++sz)
		{
			if (find(k.begin(), k.end(), szintek[sz]->getKcio()) != k.end())
			{
				tagok[t]->setSzint(szintek[sz]);
				break;
			}
		}
	}
}

////// getterek //////
const unsigned& Halozat::getMaxKozv() const
{
	return maxKozvetlen;
}

const string& Halozat::getNev() const
{
	return nev;
}

JutalekSzamito& Halozat::accessJutSzamito()
{
	return *jutSzamito;
}

const KompenzaciosTerv& Halozat::getKompTerv() const
{
	return kterv;
}

PontSzamito& Halozat::accessPontSzamito()
{
	return *pontSzamito;
}

Kvalifikacio* Halozat::accessUtolsoKcio()
{
	return kciok.back();
}

////// setterek //////
void Halozat::setNev(const char *set)
{
	nev = string(set);
}

void Halozat::setKotCsomag(const bool& set)
{
	kotelezoCsomag = set;
}

void Halozat::setPontertek(const double& set)
{
	pontertek = set;
}

void Halozat::setMaxKozvetlen(const unsigned& set)
{
	maxKozvetlen = set;
}

void Halozat::setKompTerv(const KompenzaciosTerv& k)
{
	kterv = k;
}

void Halozat::setJutSzamito(JutalekSzamito *minta)
{
	if (minta != NULL) jutSzamito = minta;
	else throw "Nem letezo jutalek szamito!";
}

void Halozat::setPontSzamito(PontSzamito *minta)
{
	pontSzamito = minta;
}

///// adderek /////
void Halozat::adKcio(Kvalifikacio *k)
{
	if (k != NULL) kciok.push_back(k);
	else throw "Nem letezo kvalifikacio!";
}

void Halozat::adSzint(Szint *sz)
{
	if (sz != NULL) szintek.push_back(sz);
	else throw "Nem letezo szint!";
}

void Halozat::adKotCsomag(Csomag *csomag)
{
	if (csomag != NULL)
	{
		kotelezoCsomagok.push_back(csomag);
		kotelezoCsomag = true;
	}
	else throw "Nem letezo kotelezo csomag!";
}

void Halozat::adAjanlottCsomag(Csomag *csomag)
{
	if (csomag != NULL) ajanlottCsomagok.push_back(csomag);
	else throw "Nem letezo ajanlott csomag!";
}
