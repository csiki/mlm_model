#include "stdafx.h"
#include "main_header.h"
#include "class_fogyaszto.h"
#include "structs.h"

///// workerek /////
void Fogyaszto::Nullaz()
{
	elkoltottPluszPenz = 0;
	forgalom.bev = forgalom.ki = 0;
	vector<Fogyasztas>::iterator it;
	for (it=fogy.begin(); it<fogy.end(); ++it)
		it->db = 0;
}

double& Fogyaszto::Vasarol(Csomag *cs)
{
	if (cs == NULL) return forgalom.ki;
	
	bool megvett = true;

	vector<Csomag*>::iterator it;
	for (it=nemFogy.begin(); it<nemFogy.end() && megvett; ++it)
	{
		if ((*it) == cs)
		{
			nemFogy.erase(it);
			megvett = false;
		}
	}

	bool talalt = false;
	if (megvett)
	{
		vector<Fogyasztas>::iterator itF;
		for (itF=fogy.begin(); itF<fogy.end() && !talalt; ++itF)
		{
			if (itF->csomag == cs)
			{
				++itF->db;
				talalt = true;
			}
		}
	}

	if (!megvett || !talalt)
	{
		Fogyasztas f;
		f.csomag = cs;
		f.db = 0;
		f.like = Fogyaszto::Tetszik(cs->ertekArArany);
		fogy.push_back(f);
	}

	forgalom.ki += cs->ar;

	return forgalom.ki;
}

bool Fogyaszto::Kiprobal(double *elkoltott)
{
	if (nemFogy.size() > 0)
	{
		vector<Csomag*>::iterator it;
		unsigned r = rand() % nemFogy.size();

		Csomag *cs = (*(nemFogy.begin() + r));
		
		Fogyasztas f;
		f.csomag = cs;
		f.db = 1;
		f.like = Fogyaszto::Tetszik(cs->ertekArArany);
		
		forgalom.ki += cs->ar;
		fogy.push_back(f);
		nemFogy.erase(nemFogy.begin() + r);
		
		*elkoltott = forgalom.ki;
		return true;
	}
	else *elkoltott = forgalom.ki;
	return false;
}

void Fogyaszto::TobbletFogyaszt()
{
	if (fogy.size() > 0)
	{
		unsigned r = rand() % fogy.size();
		++fogy[r].db;
		forgalom.ki += fogy[r].csomag->ar;
		elkoltottPluszPenz += fogy[r].csomag->ar;
	}
	else
	{
		double seged = 0;
		Kiprobal(&seged);
	}
}

bool Fogyaszto::Tetszik(const double& arany)
{
	return (rand() % 20) < arany;
}

double& Fogyaszto::RendszeresFogyaszt()
{
	if (fogy.size() > 0)
	{
		vector<Fogyasztas>::iterator it;
		for (it=fogy.begin(); it<fogy.end(); ++it)
		{
			if (it->like)
			{
				forgalom.ki += it->csomag->ar;
				++it->db;
			}
		}
	}

	return forgalom.ki;
}

unsigned Fogyaszto::FogyPontSzamol()
{
	unsigned fogypont = 0;
	
	vector<Fogyasztas>::iterator it;
	for (it = fogy.begin(); it<fogy.end(); ++it)
		if (it->db != 0)
			fogypont += it->db * it->csomag->pont;
	
	return fogypont;
}


///// getterek /////
unsigned Fogyaszto::getId() const
{
	return id;
}

vector<Fogyasztas>& Fogyaszto::getFogyasztas()
{
	return fogy;
}

Forgalom& Fogyaszto::getForgalom()
{
	return forgalom;
}

Forgalom& Fogyaszto::getOsszForgalom()
{
	return osszForgalom;
}

Tag* Fogyaszto::getSponsor() const
{
	return sponsor;
}

const unsigned& Fogyaszto::getBelepIdoszak() const
{
	return belepIdoszak;
}

const double& Fogyaszto::getElkoltottPlusz() const
{
	return elkoltottPluszPenz;
}