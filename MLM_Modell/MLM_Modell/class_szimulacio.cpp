#include "stdafx.h"
#include "main_header.h"
#include "class_szimulacio.h"

Szimulacio::~Szimulacio()
{
}

///// workerek /////
void Szimulacio::Start(unsigned *aktualisIdosz, const unsigned& ido /*= 1*/, const bool& ment /*= false*/, const bool& kiirat /*= false*/, ostream& os /*= cout*/)
{
	*aktualisIdosz = idoszak;

	halozat->PontSzamitonakIdoszakAtad(&idoszak);

	if (!halozatLementve && ment)
	{
		MentHalozat();
		MentVallalat();
	}

	if (idoszak == 0)
		tagfelv->NulladigFelvetel(tagok);

	for (int i=0; i<ido; ++i)
	{
		(*aktualisIdosz) = ++idoszak;
		Futtat();
		if (ment) MentAllapot();
		if (kiirat) Kiirat(os);
	}
}

void Szimulacio::Futtat()
{
	// bev�telek null�z�sa
	Nullazas();
	
	// tagok felv�tele, fogyasztok felv�tele
	tagfelv->Felvetel(tagok, idoszak, halozat);
	fogyfelv->Felvetel(tagok, fogyasztok, idoszak);
	
	// fogyaszt�s
	forgGen->General(tagok, fogyasztok);
	
	// levelek megkeres�se
	levelek.clear();
	for (unsigned i=0; i<tagok.size(); ++i)
		if (tagok[i]->getKozvetlenDb() == 0)
			levelek.push_back(tagok[i]);
	
	// pontsz�m�t�s
	halozat->accessPontSzamito().Szamitas(levelek, halozat->accessPontSzamito());
	
	// kvalifik�ci�k kioszt�sa
	halozat->KciokKiosztasa(tagok);

	// szintek kiosztasa
	halozat->SzintekKiosztasa(tagok);
	
	// jutalekjog oszt�s
	for (unsigned i=0; i<tagok.size(); ++i)
	{
		tagok[i]->NullazKiszamoltLab();
		halozat->accessJutSzamito().Jogosultsagok(tagok[i]);
	}

	// jutal�k oszt�s
	halozat->accessJutSzamito().Szamitas(levelek, halozat->accessJutSzamito());
	
	// v�ll. forgalma
	vallalat->ForgalomSzamitas(tagok,fogyasztok);
}

void Szimulacio::Kiirat(ostream& os /*= cout*/)
{
	double osszfogykiadas, ossztagkiadas, tagpontok, tagbevetel, nyerarany;
	unsigned elfogytermek, bluednum = 0, nyernum = 0;
	osszfogykiadas = ossztagkiadas = tagpontok = tagbevetel = nyerarany = 0.0;
	elfogytermek = 0;

	// szint-bev�tel �sszevet�sek
	unsigned num[6];
	for (unsigned i=0; i<6; ++i) num[i] = 0;
	long double bev[6];
	for (unsigned i=0; i<6; ++i) bev[i] = 0.0;
	string szintek[6];
	szintek[0] = "Executive";
	szintek[1] = "Gold";
	szintek[2] = "Lapis";
	szintek[3] = "Ruby";
	szintek[4] = "Emerald";
	szintek[5] = "Diamond";

	for (unsigned t=0; t<tagok.size(); ++t)
	{
		ossztagkiadas += tagok[t]->getForgalom().ki;
		tagbevetel += tagok[t]->getForgalom().bev;
		for (unsigned cs=0; cs<tagok[t]->getFogyasztas().size(); ++cs)
			elfogytermek += tagok[t]->getFogyasztas()[cs].db;
		tagpontok += tagok[t]->getOsszPont();
		if (tagok[t]->getSzint()->getNev() == "Blue Diamond")
			++bluednum;
		if (tagok[t]->getOsszForgalom().bev > tagok[t]->getOsszForgalom().ki) ++nyernum;

		// szint-bev�tel �sszevet�sek
		unsigned sz = 0;
		while (sz < 6)
		{
			if (tagok[t]->getSzint()->getNev() == szintek[sz])
			{
				++num[sz];
				bev[sz] += tagok[t]->getForgalom().bev;
				break;
			}
			++sz;
		}
	}

	nyerarany = nyernum / double(tagok.size()) * 100.0;

	for (unsigned f=0; f<fogyasztok.size(); ++f)
	{
		osszfogykiadas += fogyasztok[f]->getForgalom().ki;
		for (unsigned cs=0; cs<fogyasztok[f]->getFogyasztas().size(); ++cs)
			elfogytermek += fogyasztok[f]->getFogyasztas()[cs].db;
	}

	os << endl << "Kiertekeles - " << idoszak << ". idoszak\n============" << endl;
	os << "Tagok szama: " << tagok.size() << endl;
	os << "Fogyasztok szama: " << fogyasztok.size() << endl;
	os << "Tagok kiadasai: " << fixed << setprecision(2) << ossztagkiadas << " Ft" << endl;
	os << "Tagok bevetele: " << tagbevetel << endl;
	os << "Fogyasztok kiadasai: " << osszfogykiadas << " Ft" << endl;
	os << "Tagok osszpontszama: " << tagpontok << endl;
	os << "Eladott termekek szama: " << elfogytermek << " db" << endl;
	os << "Blue Diamond szintuek szama: " << bluednum << " fo" << endl;
	os << "Nyeresegesek szama: " << nyernum << " fo" << endl;
	os << "Nyeresegesek aranya: " << nyerarany << "%" << endl;
	os << vallalat->getNev() << " profitja: " << vallalat->getForgalom().bev - vallalat->getForgalom().ki << endl;

	// szint-bev�tel �sszevet�sek
	for (unsigned i=0; i<6; ++i)
	{
		os << szintek[i] << " atlagbevetel: " << ((num[i] == 0) ? 0 : (bev[i] / num[i])) << endl;
	}
}

// perzisztencia
void Szimulacio::MentHalozat()
{
}

void Szimulacio::MentAllapot() // v�llalat v�ltoz� adatait is menti
{
}

void Szimulacio::MentVallalat()
{
}

bool Szimulacio::Betolt(const char *mappanev)
{
	// ...
	halozatLementve = true;
	return true;
}

// bels� workerek
bool Szimulacio::Betolt(const char *allapotFajlNev, const char *halozatFajlNev, const char *vallalatFajlNev)
{
	// ...
	return true;
}

void Szimulacio::Nullazas()
{
	vector<Tag*>::iterator tIt;
	vector<Fogyaszto*>::iterator fIt;

	for (tIt = tagok.begin(); tIt < tagok.end(); ++tIt)
		(*tIt)->Nullaz();
	for (fIt = fogyasztok.begin(); fIt < fogyasztok.end(); ++fIt)
		(*fIt)->Nullaz();
}