#ifndef _CLASS_FOGYASZTO_
#define _CLASS_FOGYASZTO_

#include "main_header.h"
#include "structs.h"

class Fogyaszto
{
	protected:
		unsigned id;
		unsigned belepIdoszak; // 0 a h�l�zat kezd�tagjai
		double elkoltottPluszPenz; // kotelezo teljes�t�s��rt beh�vott tagok sz�ma
		Tag *sponsor;
		vector<Fogyasztas> fogy;
		vector<Csomag*> nemFogy;
		Forgalom forgalom; // adott id�szakban
		Forgalom osszForgalom;

	public:
		Fogyaszto(const unsigned& id, const unsigned& belepIdoszak, Tag *sponsor, vector<Csomag*>& osszCsomag) : 
		  id(id),
		  belepIdoszak(belepIdoszak),
		  sponsor(sponsor)
		{
			forgalom.bev = forgalom.ki = osszForgalom.bev = osszForgalom.ki = 0;
			elkoltottPluszPenz = 0;
			nemFogy = osszCsomag;
		}

		// workerek
		virtual void Nullaz();
		double& Vasarol(Csomag *cs); // return: eddig k�lt�tt; csak bel�ptet�sre haszn�ld (lass�)
		bool Kiprobal(double *elkoltott); // a nem fogyasztottak k�z�l v�laszt
		double& RendszeresFogyaszt();
		void TobbletFogyaszt();
		static bool Tetszik(const double& arany); // ha ar=ertek 50%
		unsigned FogyPontSzamol();

		// getterek
		unsigned getId() const;
		const unsigned& getBelepIdoszak() const;
		Tag *getSponsor() const;
		Fogyasztas& getFogyasztas(const unsigned& index, bool *letezik) const;
		Forgalom& getForgalom();
		Forgalom& getOsszForgalom();
		Csomag* getNemFogy(const unsigned& index, bool *letezik) const;
		vector<Fogyasztas>& getFogyasztas();
		const double& getElkoltottPlusz() const;

		// setterek
		//void setId(const unsigned& set);
		//void setBelepIdoszak(const unsigned& set);
		void setSponsor(Tag *set);
		void setFogy(Fogyasztas minta, const unsigned& index);
		void setForgalom(const Forgalom& minta);
		void setNemFogy(Csomag *minta, const unsigned& index);
		//void setOsszForgalom(const Forgalom& minta);

		// adderek
		void adFogyasztas(const Fogyasztas& minta);
		void adNemFogy(Csomag *minta);

		// removerek
		void rmvNemFogy(const unsigned& index);
};

#endif