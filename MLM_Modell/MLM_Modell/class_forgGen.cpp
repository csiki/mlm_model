#include "stdafx.h"
#include "main_header.h"
#include "class_forgGen.h"
#include "class_tag.h"

// workerek
void ForgalomGeneralo::General(vector<Tag*>& tagok, vector<Fogyaszto*>& fogyasztok)
{
	vector<Tag*>::iterator t;
	vector<Fogyaszto*>::iterator f;
	double elkoltott = 0;

	for (t=tagok.begin(); t<tagok.end(); ++t)
	{
		if ( (maxKolt - (*t)->RendszeresFogyaszt()) > 0 )
			while((*t)->Kiprobal(&elkoltott) && ((maxKolt - elkoltott) > 0));
	}
	for (f=fogyasztok.begin(); f<fogyasztok.end(); ++f)
	{
		if ( (maxKolt - (*f)->RendszeresFogyaszt()) > 0 )
			while((*f)->Kiprobal(&elkoltott) && ((maxKolt - elkoltott) > 0));
	}
}

// getter
const unsigned& ForgalomGeneralo::getMaxKolt() const
{
	return maxKolt;
}

const unsigned& ForgalomGeneralo::getTermekProbal() const
{
	return dbTermekProbal;
}

// setter
void ForgalomGeneralo::setMaxKolt(const unsigned& set)
{
	maxKolt = set;
}

void ForgalomGeneralo::setTermekProbal(const unsigned& set)
{
	dbTermekProbal = set;
}