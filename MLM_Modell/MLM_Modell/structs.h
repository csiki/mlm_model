#ifndef _STRUCTS_
#define _STRUCTS_

#include "main_header.h"

struct Melyseg
{
	unsigned tol, ig;  // ha ig=0, végtelen mélység
};

struct Csomag
{
	string nev;
	bool bonus;
	double ertek;
	double ar;
	double ertekArArany; // ertek/ar * 10 (számoláshoz)
	double pont;

	Csomag(const char *nev, const bool& bonus, const double& ar, const double& ertek, const double& pont)
		:nev(nev), bonus(bonus), ertek(ertek), ar(ar), pont(pont)
	{
		SzamolArany();
	}

	void setErtek(const double& e)
	{
		ertek = e;
		SzamolArany();
	}

	void setAr(const double& a)
	{
		ar = a;
		SzamolArany();
	}

	void SzamolArany()
	{
		ertekArArany = ertek / ar * 10;
	}
};

struct Fogyasztas
{
	Csomag *csomag;
	unsigned db; // adott időszakban
	bool like;
};

struct Forgalom
{
	double bev;
	double ki;
};

#endif