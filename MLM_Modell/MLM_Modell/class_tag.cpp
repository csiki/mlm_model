#include "stdafx.h"
#include "main_header.h"
#include "class_tag.h"
#include "class_kvalifikacio.h"


///// workerek //////
void Tag::Felvetel(Tag *tag, const KompenzaciosTerv& kterv, const unsigned& maxKozvetlen)
{
	kozvetlenek.push_back(tag);
	if ( (maxKozvetlen == 0) && (kterv == BREAKAWAY || kterv == UNILEVEL) )
		alatta.push_back(tag);
	else if (labPont.size() > 0)
	{
		Beillesztes(FelvetelElve(), tag);
	}
	else 
	{
		alatta.push_back(tag);
	}
}

const unsigned& Tag::FelvetelElve() /// Gyeng�bb l�b elv�n
{
	unsigned index = 0;
	for (unsigned l=1; l<labPont.size(); ++l)
		if (labPont[l] > labPont[index])
			index = l;
	return index;
}

void Tag::Beillesztes(const unsigned& lab, Tag *tag)
{
	if (kozvetlenek.size() <= lab)
	{
		alatta.push_back(tag);
		return;
	}
	kozvetlenek[lab]->Beillesztes(FelvetelElve(), tag);
}

void Tag::KciokKiosztasa()
{
	vector<Kvalifikacio*>::iterator it;

	// megl�v� kci�k
	it = kciok.begin();
	while( it != kciok.end() )
	{
		if (!(*it)->Statikus() && !(*it)->Kvalifalt(this))
		{
			(*it)->athelyezni = false;
			nemKvalifalt.push_back(*it);
			it = kciok.erase(it);
		}
		else ++it;
	}

	// �jabb kci�k
	it = nemKvalifalt.begin();
	while( it != nemKvalifalt.end() )
	{
		if ((*it)->Kvalifalt(this))
		{
			kciok.push_back(*it);
			it = nemKvalifalt.erase(it);
		}
		else ++it;
	}

	// al�rendelt
	it = kciok.begin();
	while( it != kciok.end() )
	{
		if ((*it)->athelyezni)
		{
			(*it)->athelyezni = false;
			nemKvalifalt.push_back(*it);
			it = kciok.erase(it);
		}
		else ++it;
	}

	it = nemKvalifalt.begin();
	while( it != nemKvalifalt.end() )
	{
		if ((*it)->athelyezni)
		{
			(*it)->athelyezni = false;
			kciok.push_back(*it);
			it = nemKvalifalt.erase(it);
		}
		else ++it;
	}

	//cout << kciok.size() << "size" << endl;
}

void Tag::Nullaz()
{
	forgalom.bev = forgalom.ki = 0;
	pontBonus = sajatPont = 0;
	elkoltottPluszPenz = 0;
	kiszamoltLab = 0;
	fogyasztokPontja = 0;
	fogyasztokKiadasa = 0;
	labPont.clear();
	jutalekok.clear();
	genKiadas.clear();

	vector<Fogyasztas>::iterator it;
	for (it=fogy.begin(); it<fogy.end(); ++it)
		it->db = 0;
}

bool Tag::novelKiszamoltLab()
{
	if (++kiszamoltLab < kozvetlenek.size())
		return true;
	return false;
}

void Tag::NullazKiszamoltLab()
{
	kiszamoltLab = 0;
}

void Tag::SajatPontSzamol()
{
	sajatPont = this->FogyPontSzamol();
}

void Tag::KozvetlenPontSzamol(vector<Szint*>& megengedettSzintek)
{
	if (megengedettSzintek.size() == 0)
	{
		for (unsigned k=0; k<kozvetlenek.size(); ++k)
		{
			labPont[k] = kozvetlenek[k]->getPont();
		}
	}
	else
	{
		for (unsigned k=0; k<kozvetlenek.size(); ++k)
		{
			if ( std::find(megengedettSzintek.begin(), megengedettSzintek.end(), kozvetlenek[k]->getSzint()) != megengedettSzintek.end() )
				labPont.push_back(kozvetlenek[k]->getPont());
			else labPont.push_back(0);
		}
	}
}

void Tag::FogyasztokPontSzamol()
{
	for (unsigned f=0; f<fogyasztoi.size(); ++f)
		fogyasztokPontja += fogyasztoi[f]->FogyPontSzamol();
}

void Tag::OsszPontSzamol()
{
	osszPont += pontBonus + sajatPont + fogyasztokPontja + accumulate(labPont.begin(), labPont.end(), 0);
}

void Tag::FogyasztokKiadasSzamol()
{
	for (unsigned f=0; f<fogyasztoi.size(); ++f)
		fogyasztokKiadasa += fogyasztoi[f]->getForgalom().ki;
}

void Tag::KiadasokAtVesz(deque<map<Szint*, double> >& alattaGenKiadas)
{
	for (unsigned d=0; d<alattaGenKiadas.size(); ++d)
	{
		if (d >= genKiadas.size()) genKiadas.push_front(alattaGenKiadas[d]);
		else
		{
			map<Szint*, double>::iterator aIt;
			map<Szint*, double>::iterator talalat;
			for (aIt=alattaGenKiadas[d].begin(); aIt != alattaGenKiadas[d].end(); ++aIt)
			{
				talalat = genKiadas[d].find(aIt->first);
				if (talalat != genKiadas[d].end())
					talalat->second += aIt->second;
				else
					genKiadas[d].insert(pair<Szint*,double>(aIt->first,aIt->second));
			}
		}
	}
}

void Tag::OsszForgalomNovel()
{
	osszForgalom.ki += forgalom.ki;
}


///// getterek //////
const vector<double>& Tag::getLabPont() const
{
	return labPont;
}

vector<double>& Tag::accessLabPont()
{
	return labPont;
}

const unsigned& Tag::getKozvetlenDb() const
{
	return kozvetlenek.size();
}

vector<Tag*> Tag::accessKozvetlenek()
{
	return kozvetlenek;
}

Szint* Tag::getSzint() const
{
	return szint;
}

vector<Kvalifikacio*>& Tag::accessKciok()
{
	return kciok;
}

vector<Kvalifikacio*>& Tag::accessNemKvalifalt()
{
	return nemKvalifalt;
}

const double& Tag::getSajatPont() const
{
	return sajatPont;
}

double Tag::getPont() const // + sajatPont + fogyasztokPontja
{
	return accumulate(labPont.begin(), labPont.end(), 0) + sajatPont + fogyasztokPontja;
}

double Tag::getOsszPont()
{
	return osszPont;
}

double Tag::getOsszKiadas()
{
	return forgalom.ki + fogyasztokKiadasa;
}

vector<Jutalek*>& Tag::accessJutalekok()
{
	return jutalekok;
}


///// setterek /////
void Tag::setSzint(Szint *set)
{
	if (set == NULL) throw "Nem letezo Szint!";
	szint = set;
}

///// adderek /////
void Tag::adFogyaszto(Fogyaszto *fogy)
{
	fogyasztoi.push_back(fogy);
}

void Tag::adJutalek(Jutalek *jut)
{
	if (jut != NULL) jutalekok.push_back(jut);
	else throw "Nem letezo jutalek!";
}