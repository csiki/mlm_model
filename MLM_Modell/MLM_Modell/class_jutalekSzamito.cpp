#include "stdafx.h"
#include "main_header.h"
#include "class_jutalekSzamito.h"
#include "class_tag.h"
#include "class_jutalek.h"
#include <boost\thread.hpp>
#include <threadpool.hpp>
#include "class_szint.h"

boost::mutex genKiMutex;

///// workerek /////
void JutalekSzamito::Szamitas(vector<Tag*>& levelek, JutalekSzamito& peldany)
{
	boost::threadpool::pool tp(40);

	for (unsigned l=0; l<levelek.size(); ++l)
	{
		tp.schedule(boost::bind(&JutalekSzamito::Szamit, this, levelek[l]));
	}	
}

void JutalekSzamito::Szamit(Tag *tag)
{
	if (tag->novelKiszamoltLab())
	{
		return;
	}

	boost::mutex::scoped_lock genLock(genKiMutex, boost::defer_lock);

	// Fogyasztóinak költekezése
	tag->FogyasztokKiadasSzamol();

	genLock.lock();
	map<Szint*,double> minta;
	minta.insert(pair<Szint*,double>(tag->getSzint(), tag->getOsszKiadas()));
	tag->genKiadas.push_front(minta);
	genLock.unlock();

	genLock.lock();
	if (tag->genKiadas.size() > maxGen) tag->genKiadas.pop_back();
	genLock.unlock();

	// Jutalélkok
	vector<Jutalek*>& jutalekok = tag->accessJutalekok();
	vector<Jutalek*>::iterator it;
	for (it=jutalekok.begin(); it<jutalekok.end(); ++it)
	{
		tag->getForgalom().bev += (*it)->Kiszamol(tag);
	}

	tag->getOsszForgalom().bev += tag->getForgalom().bev;

	if (tag->getSponsor() != NULL)
	{
		genLock.lock();
		tag->getSponsor()->KiadasokAtVesz(tag->genKiadas);
		genLock.unlock();

		Szamit(tag->getSponsor());
	}
}

void JutalekSzamito::Jogosultsagok(Tag *tag)
{
	vector<Kvalifikacio*> kciok = tag->accessKciok();
	
	for (unsigned j=0; j<jutalekok.size(); ++j)
		if ( find(kciok.begin(), kciok.end(), jutalekok[j]->getKcio() ) != kciok.end() )
			tag->adJutalek(jutalekok[j]);
}

///// getterek /////

///// adderek /////
void JutalekSzamito::adJutalek(Jutalek *jut)
{
	jutalekok.push_back(jut);
	if (jut->getVegtelenMely()) vegtelenGen = true;
	if (jut->getMelyseg().ig > maxGen) maxGen = jut->getMelyseg().ig;
}