#ifndef _CLASS_FOGYASZTO_FELVETEL_
#define _CLASS_FOGYASZTO_FELVETEL_

#include "main_header.h"
#include "structs.h"

class FogyasztoFelvetel // �r�k�thet�
{
	protected:
		double atlagBehivott;
		vector<Csomag*> osszCsomag; // Fogyaszto::nemFogy felt�lt�s�hez
	public:

		FogyasztoFelvetel(const double& atlagBehivott) : atlagBehivott(atlagBehivott) {}

		// workerek
		void Felvetel(vector<Tag*>& tagok, vector<Fogyaszto*>& fogyasztok, const unsigned& idoszak);
		void MasolCsomagok(vector<Csomag*>& csomagok);

		// getter
		const double& getAtlagBehivott() const;

		// setter
		void setAtlagBehivott(const double& set);
};

#endif