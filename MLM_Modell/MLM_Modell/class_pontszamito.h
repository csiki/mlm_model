#ifndef _CLASS_PONTSZAMITO_
#define _CLASS_PONTSZAMITO_

#include "main_header.h"

class PontSzamito // �r�k�thet�
{
	protected:
		vector<Szint*> megengedettSzintek; // ha 0 m�ret�, akkor minden szint elfogadott
		bool kotelezoHavi;
		vector<double> kotelezoHavik;
		unsigned *idoszak;
	public:
		PontSzamito() : kotelezoHavi(false), idoszak(NULL) {}

		// workerek
		virtual void Szamitas(vector<Tag*>& levelek, PontSzamito& peldany);
		virtual void Szamit(Tag *tag);
		const double& AktKotelezoHavi(const unsigned& belep);

		// getterek
		const bool& getKotHavi() const;

		// setterek
		//void setKotHavi(const bool& set);
		void setIdoszak(unsigned *i);

		// adderek
		void adMegengedettSzint(Szint *szint);
		void adKotHavi(const double& kothavi, const unsigned& idoszak); // ha idoszak=0, akkor ez az �sszes id�szakra vonatkozik, kiv�ve amit k�l�n megadtunk m�g
};

#endif