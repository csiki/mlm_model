#include "stdafx.h"
#include "main_header.h"
#include "structs.h"
#include "class_fogyaszto.h"
#include "class_kvalifikacio.h"
#include "class_jutalek.h"
#include "class_szint.h"
#include "class_vallalat.h"
#include "class_halozat.h"
#include "class_szimulacio.h"
#include "class_jutalekSzamito.h"
#include "class_forgGen.h"
#include "class_tag.h"

void WaitForKey();

void NuSkinBetolt(Halozat **Nuskin, Vallalat **NusEnt, TagFelvetel **NUStagfelv, FogyasztoFelvetel **NUSfogyfelv, ForgalomGeneralo **NUSforggen);

int main(int argc, char* argv[])
{
	Halozat *Nuskin;
	Vallalat *NusEnt;
	unsigned idosz;

	srand((unsigned)time(0));

	try {
		Nuskin = new Halozat();
		NusEnt = new Vallalat("NuSkin Enterprises");
		TagFelvetel *NUStagfelv;
		ForgalomGeneralo *NUSforggen;
		FogyasztoFelvetel *NUSfogyfelv;

		NuSkinBetolt(&Nuskin, &NusEnt, &NUStagfelv, &NUSfogyfelv, &NUSforggen);
		
		Nuskin->BiztositKciok(NUStagfelv);
		NusEnt->BiztositCsomagok(NUStagfelv, NUSfogyfelv);

		/// Szimuláció ///
		Szimulacio szim(Nuskin, NusEnt, NUStagfelv, NUSfogyfelv, NUSforggen, "NuSkin1.0");
		szim.Start(&idosz,100,true,true);
	}
	catch(char *e)
	{
		cout << e << endl;
	};

	WaitForKey();

	return 0;
}

void WaitForKey()
{
    char key = 0;
    while ( !key )
        cin.get(key);
}