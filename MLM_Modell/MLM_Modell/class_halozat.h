#ifndef _CLASS_HALOZAT_
#define _CLASS_HALOZAT_

#include "main_header.h"
#include "class_vallalat.h"
#include "class_jutalek.h"
#include "class_szint.h"
#include "class_fogyaszto.h"
#include "class_forgGen.h"
#include "class_jutalekSzamito.h"
#include "class_tagFelvetel.h"
#include "class_jutalekSzamito.h"
#include "class_pontszamito.h"

class Halozat // örökíthető
{
	protected:
		string nev;
		vector<Szint*> szintek; // 0. az alapszint
		vector<Kvalifikacio*> kciok;
		bool kotelezoCsomag;
		vector<Csomag*> kotelezoCsomagok;
		vector<Csomag*> ajanlottCsomagok;
		double pontertek;
		unsigned maxKozvetlen;
		JutalekSzamito *jutSzamito;
		PontSzamito *pontSzamito;
		KompenzaciosTerv kterv;

	public:
		Halozat();
		Halozat(const char* nev);
		virtual ~Halozat();

		// workerek
		void KciokKiosztasa(vector<Tag*>& tagok); // + levelek megkeresése
		Csomag *RandCsomag() const;
		void BiztositKciok(TagFelvetel * tagfelv);
		void PontSzamitonakIdoszakAtad(unsigned *i);
		void SzintekKiosztasa(vector<Tag*>& tagok);

		// getterek
		const unsigned& getMaxKozv() const;
		const string& getNev() const;
		const Szint& getSzint(const unsigned& index, bool *letezik) const;
		const double& getPontertek() const;
		const Kvalifikacio& getKcio(const unsigned& id, bool *letezik) const;
		const JutalekSzamito& getJutSzamito() const;
		const bool& KotCsomag();
		Csomag *getKotCsomag(const unsigned& index, bool *letezik) const;
		Csomag *getAjanlottCsomag(const unsigned& index, bool *letezik) const;
		JutalekSzamito& accessJutSzamito();
		PontSzamito& accessPontSzamito();
		const KompenzaciosTerv& getKompTerv() const;
		Kvalifikacio* accessUtolsoKcio();

		// setterek
		void setMaxKozvetlen(const unsigned& set);
		void setNev(const char *set);
		void setSzint(const Szint& minta, const unsigned& index);
		void setPontertek(const double& set);
		void setKcio(const Kvalifikacio& minta, const unsigned& index);
		void setJutSzamito(JutalekSzamito *minta);
		void setPontSzamito(PontSzamito *minta);
		void setKotCsomag(const bool& set);
		void setKotCsomag(Csomag *minta, const unsigned& index);
		void setAjanlottCsomag(Csomag *minta, const unsigned& index);
		void setKompTerv(const KompenzaciosTerv& k);

		// adderek
		void adKcio(Kvalifikacio *k);
		void adSzint(Szint *sz);
		void adKotCsomag(Csomag *csomag);
		void adAjanlottCsomag(Csomag *csomag);

		// removerek
		void rmvKotCsomag(const unsigned& index);
		void rmvKotCsomag(Csomag *minta);
		void rmvAjanCsomag(const unsigned& index);
		void rmvAjanCsomag(Csomag *minta);
};

#endif