#ifndef _CLASS_TAG_
#define _CLASS_TAG_

#include "main_header.h"
#include "class_fogyaszto.h"

class Tag : public Fogyaszto
{
	private:
		Tag *felette; // gr�fban f�l�tte l�v� Tag
		Szint *szint;
		vector<Kvalifikacio*> kciok;
		vector<Kvalifikacio*> nemKvalifalt;
		vector<Fogyaszto*> fogyasztoi;
		vector<Tag*> kozvetlenek;
		vector<Tag*> alatta; // alatt l�v� tagok
		vector<Jutalek*> jutalekok;
		
		vector<double> labPont;
		double pontBonus; // amib�l a fels�vonal nem kap sz�zal�kot
		double sajatPont;
		double osszPont; // eddigi idoszakban osszeszedett �sszes
		double fogyasztokPontja;
		double fogyasztokKiadasa;
		unsigned kiszamoltLab; // pontsz�m�t�skor
	public:
		deque<map<Szint*, double> > genKiadas;

		Tag(const unsigned& id, const unsigned& belepIdoszak, Tag *sponsor, vector<Csomag*>& osszCsomag, Tag *felette, Szint *szint, const vector<Kvalifikacio*>& osszKcio) :
		  Fogyaszto(id, belepIdoszak, sponsor, osszCsomag),
		  felette(felette),
		  szint(szint),
		  nemKvalifalt(osszKcio)
		  {
			sajatPont = 0;
			pontBonus = 0;
			kiszamoltLab = 0;
			fogyasztokPontja = 0;
			fogyasztokKiadasa = 0;
			osszPont = 0;
		  }

		// workerek
		void KciokKiosztasa();
		void Felvetel(Tag *tag, const KompenzaciosTerv& kterv, const unsigned& maxKozvetlen); // meg is adja tag-nak, hogy ki van f�l�tte
		virtual const unsigned& FelvetelElve(); // kiv�lasztja melyik l�tez�(!) l�bra �rdemes tenni
		virtual void Nullaz();
		virtual void SajatPontSzamol();
		virtual void KozvetlenPontSzamol(vector<Szint*>& megengedettSzintek);
		virtual void FogyasztokPontSzamol();
		virtual void OsszPontSzamol();
		virtual void Beillesztes(const unsigned& lab, Tag *tag);
		virtual void FogyasztokKiadasSzamol();
		void KiadasokAtVesz(deque<map<Szint*, double> >& alattaGenKiadas);
		void OsszForgalomNovel();

		// getterek
		Tag *getFelette() const;
		Szint *getSzint() const;
		Kvalifikacio *getKcio(const unsigned& index, bool *letezik) const;
		Fogyaszto *getFogyaszto(const unsigned& index, bool *letezik) const;
		Tag *getKozvetlen(const unsigned& index, bool *letezik) const;
		Tag *getAlatta(const unsigned& index, bool *letezik) const;
		const double& getBonus() const;
		const double& getSajatPont() const;
		virtual double getPont() const;
		const vector<double>& getLabPont() const;
		const unsigned& getKozvetlenDb() const;
		vector<Tag*> accessKozvetlenek();
		vector<Kvalifikacio*>& accessKciok();
		vector<Kvalifikacio*>& accessNemKvalifalt();
		vector<double>& accessLabPont();
		double getOsszPont();
		double getOsszKiadas();
		vector<Jutalek*>& accessJutalekok();

		// setterek
		void setFelette(Tag *set);
		void setSzint(Szint *set);
		void setKcio(Kvalifikacio * set, const unsigned& index);
		void setFogyaszto(Fogyaszto *set, const unsigned& index);
		void setKozvetlen(Tag *set, const unsigned& index);
		void setAlatta(Tag *set, const unsigned& index);
		void setBonus(const unsigned& set);
		void setPont(const unsigned& set);
		void NullazKiszamoltLab();

		// adderek
		void adKcio(Kvalifikacio *kcio);
		void adFogyaszto(Fogyaszto *fogy);
		void adKozvetlen(Tag *kozv);
		void adAlatta(Tag *al);
		bool novelKiszamoltLab();
		void adJutalek(Jutalek *jut);
};

#endif