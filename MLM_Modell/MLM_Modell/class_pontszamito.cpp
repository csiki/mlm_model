#include "stdafx.h"
#include "main_header.h"
#include "class_pontszamito.h"
#include "class_tag.h"
#include "class_szimulacio.h"
#include <boost\thread.hpp>
#include <threadpool.hpp>

void PontSzamito::Szamitas(vector<Tag*>& levelek, PontSzamito& peldany)
{
	boost::threadpool::pool tp(40);

	for (unsigned l=0; l<levelek.size(); ++l)
	{
		tp.schedule(boost::bind(&PontSzamito::Szamit, this, levelek[l]));
	}
}

void PontSzamito::Szamit(Tag *tag)
{
	if (tag->novelKiszamoltLab())
	{
		return;
	}
	
	// saját pont
	tag->SajatPontSzamol();

	// fogyasztóinak a pontja
	tag->FogyasztokPontSzamol();

	// közvetlenek pontjai
	tag->KozvetlenPontSzamol(megengedettSzintek);

	// kotelezo pontok
	if (kotelezoHavi)
	{
		unsigned khavi = AktKotelezoHavi(tag->getBelepIdoszak());
		while (tag->getPont() < khavi)
		{
			tag->TobbletFogyaszt();
			tag->SajatPontSzamol();
		}
	}

	// osszpont
	tag->OsszPontSzamol();

	// osszforgalom rendez
	tag->OsszForgalomNovel();
	
	if (tag->getSponsor() != NULL)
	{
		Szamit(tag->getSponsor());
	}
}

const double& PontSzamito::AktKotelezoHavi(const unsigned& belep)
{
	unsigned kulonbseg = *idoszak - belep + 1;
	if (kotelezoHavik.size() <= kulonbseg)
		return kotelezoHavik[0];
	return kotelezoHavik[kulonbseg];
}

///// adderek /////
void PontSzamito::adMegengedettSzint(Szint *szint)
{
	megengedettSzintek.push_back(szint);
}

void PontSzamito::adKotHavi(const double& kothavi, const unsigned& idoszak)
{
	if (idoszak >= kotelezoHavik.size())
	{
		for (unsigned i=kotelezoHavik.size(); i<idoszak; ++i)
			kotelezoHavik.push_back(0);
		kotelezoHavik.push_back(kothavi);
	}
	else
	{
		kotelezoHavik[idoszak] = kothavi;
	}

	kotelezoHavi = true;
}

///// setterek /////
void PontSzamito::setIdoszak(unsigned *i)
{
	idoszak = i;
}