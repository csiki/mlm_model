#include "stdafx.h"
#include "main_header.h"
#include "class_fogyasztoFelvetel.h"
#include "class_tag.h"

// workerek
void FogyasztoFelvetel::Felvetel(vector<Tag*>& tagok, vector<Fogyaszto*>& fogyasztok, const unsigned& idoszak)
{
	unsigned fogyDb = fogyasztok.size();
	Tag *randtag;
	for (unsigned f = 0; f < (atlagBehivott*tagok.size()); ++f)
	{
		randtag = tagok[rand() % tagok.size()];
		fogyasztok.push_back(new Fogyaszto(fogyDb + f, idoszak, randtag, osszCsomag));
		randtag->adFogyaszto(fogyasztok.back());
	}
}

void FogyasztoFelvetel::MasolCsomagok(vector<Csomag*>& csomagok)
{
	osszCsomag = csomagok;
}

// getter
const double& FogyasztoFelvetel::getAtlagBehivott() const
{
	return atlagBehivott;
}

// setter
void FogyasztoFelvetel::setAtlagBehivott(const double& set)
{
	atlagBehivott = set;
}