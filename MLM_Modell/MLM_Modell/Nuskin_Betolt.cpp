#include "stdafx.h"
#include "main_header.h"
#include "structs.h"
#include "class_fogyaszto.h"
#include "class_kvalifikacio.h"
#include "class_jutalek.h"
#include "class_szint.h"
#include "class_vallalat.h"
#include "class_halozat.h"
#include "class_szimulacio.h"
#include "class_jutalekSzamito.h"
#include "class_forgGen.h"
#include "class_tag.h"
#include "class_fogyasztoFelvetel.h"
#include "class_tagFelvetel.h"

void NuSkinBetolt(Halozat **Nuskin, Vallalat **NusEnt, TagFelvetel **NUStagfelv, FogyasztoFelvetel **NUSfogyfelv, ForgalomGeneralo **NUSforggen)
{
	JutalekSzamito *NUSjut;
	PontSzamito *NUSpont;

	NUSjut = new JutalekSzamito;
	NUSpont = new PontSzamito;

	/// Vallalat, csomagok ///
	// pharmanex
	(*NusEnt)->adCsomag(new Csomag("LifePak", false, 18044, 18044, 71.10));
	(*NusEnt)->adCsomag(new Csomag("LifePak & Marine", false, 21422, 21422, 71.00));
	(*NusEnt)->adCsomag(new Csomag("Marine Omega", false, 10513, 10513, 41.40));
	(*NusEnt)->adCsomag(new Csomag("G3", false, 18641, 18641, 73.40));
	(*NusEnt)->adCsomag(new Csomag("Vitality", false, 12692, 12692, 50.00));
	(*NusEnt)->adCsomag(new Csomag("OVERdrive", false, 6487, 6487, 25.50));
	(*NusEnt)->adCsomag(new Csomag("Tegreen", false, 5841, 5841, 23.00));
	(*NusEnt)->adCsomag(new Csomag("IgG Boost", false, 13720, 13720, 54.00));
	(*NusEnt)->adCsomag(new Csomag("ReishiMax GLp", false, 22151, 22151, 87.20));
	(*NusEnt)->adCsomag(new Csomag("CordyMax Cs-4", false, 7929, 7929, 31.20));
	(*NusEnt)->adCsomag(new Csomag("BioGinkgo", false, 9227, 9227, 36.40));
	(*NusEnt)->adCsomag(new Csomag("FlexCreme", false, 5815, 5815, 22.90));
	(*NusEnt)->adCsomag(new Csomag("Flexcare", false, 10339, 10399, 40.70));
	(*NusEnt)->adCsomag(new Csomag("Estera Balance", false, 13372, 13372, 52.60));
	(*NusEnt)->adCsomag(new Csomag("Estera Maintenance", false, 13372, 13372, 52.60));
	(*NusEnt)->adCsomag(new Csomag("Optimum Omega", false, 5344, 5344, 21.00));
	// nuskin
	// ...

	/// Halozat ///
	(*Nuskin)->setNev("NuSkin");
	(*Nuskin)->setKotCsomag(false);
	(*Nuskin)->setPontertek(300);
	(*Nuskin)->setMaxKozvetlen(0);
	(*Nuskin)->setKompTerv(BREAKAWAY);

	/// Kciok
	// szintek
	Kvalifikacio *regelt = new Kvalifikacio;
	Kvalifikacio *bluediamond = new Kvalifikacio(false, 12, 50, 2000, 4500);
	Kvalifikacio *diamond = new Kvalifikacio(false, 8, 50, 2000, 4500);
	Kvalifikacio *emerald = new Kvalifikacio(false, 6, 50, 2000, 4500);
	Kvalifikacio *ruby = new Kvalifikacio(false, 4, 50, 2000, 4500);
	Kvalifikacio *lapis = new Kvalifikacio(false, 2, 50, 2000, 4500);
	Kvalifikacio *gold = new Kvalifikacio(false, 1, 50, 2000, 4500);
	Kvalifikacio *exec = new Kvalifikacio(false, 0, 50, 2000, 4500);
	Kvalifikacio *q2 = new Kvalifikacio(true, 0, 0, 0, 2500);
	Kvalifikacio *q1 = new Kvalifikacio(true, 0, 0, 0, 1000);
	Kvalifikacio *loi = new Kvalifikacio(true, 0, 0, 0, 250);
	// szint alarendeltek
	bluediamond->adAlarendeltKcio(diamond);
	bluediamond->adAlarendeltKcio(emerald);
	bluediamond->adAlarendeltKcio(ruby);
	bluediamond->adAlarendeltKcio(lapis);
	bluediamond->adAlarendeltKcio(gold);
	bluediamond->adAlarendeltKcio(exec);
	bluediamond->adAlarendeltKcio(q2);
	bluediamond->adAlarendeltKcio(q1);
	bluediamond->adAlarendeltKcio(loi);
	diamond->adAlarendeltKcio(emerald);
	diamond->adAlarendeltKcio(ruby);
	diamond->adAlarendeltKcio(lapis);
	diamond->adAlarendeltKcio(gold);
	diamond->adAlarendeltKcio(exec);
	diamond->adAlarendeltKcio(q2);
	diamond->adAlarendeltKcio(q1);
	diamond->adAlarendeltKcio(loi);
	emerald->adAlarendeltKcio(ruby);
	emerald->adAlarendeltKcio(lapis);
	emerald->adAlarendeltKcio(gold);
	emerald->adAlarendeltKcio(exec);
	emerald->adAlarendeltKcio(q2);
	emerald->adAlarendeltKcio(q1);
	emerald->adAlarendeltKcio(loi);
	ruby->adAlarendeltKcio(lapis);
	ruby->adAlarendeltKcio(gold);
	ruby->adAlarendeltKcio(exec);
	ruby->adAlarendeltKcio(q2);
	ruby->adAlarendeltKcio(q1);
	ruby->adAlarendeltKcio(loi);
	lapis->adAlarendeltKcio(gold);
	lapis->adAlarendeltKcio(exec);
	lapis->adAlarendeltKcio(q2);
	lapis->adAlarendeltKcio(q1);
	lapis->adAlarendeltKcio(loi);
	gold->adAlarendeltKcio(exec);
	gold->adAlarendeltKcio(q2);
	gold->adAlarendeltKcio(q1);
	gold->adAlarendeltKcio(loi);
	exec->adAlarendeltKcio(q2);
	exec->adAlarendeltKcio(q1);
	exec->adAlarendeltKcio(loi);
	q2->adAlarendeltKcio(q1);
	q2->adAlarendeltKcio(loi);
	q1->adAlarendeltKcio(loi);
		
	// exec bonus
	Kvalifikacio *k50 = new Kvalifikacio(false, 0, 50, 50000, 4500);
	Kvalifikacio *k25 = new Kvalifikacio(false, 0, 50, 25000, 4500);
	Kvalifikacio *k15 = new Kvalifikacio(false, 0, 50, 15000, 4500);
	Kvalifikacio *k10 = new Kvalifikacio(false, 0, 50, 10000, 4500);
	Kvalifikacio *k5 = new Kvalifikacio(false, 0, 50, 5000, 4500);
	Kvalifikacio *k3 = new Kvalifikacio(false, 0, 50, 3000, 4500);
	Kvalifikacio *k2 = new Kvalifikacio(false, 0, 50, 2000, 4500);
	// exec bonus alarendeltek
	k50->adAlarendeltKcio(k25);
	k50->adAlarendeltKcio(k15);
	k50->adAlarendeltKcio(k10);
	k50->adAlarendeltKcio(k5);
	k50->adAlarendeltKcio(k3);
	k50->adAlarendeltKcio(k2);
	k25->adAlarendeltKcio(k15);
	k25->adAlarendeltKcio(k10);
	k25->adAlarendeltKcio(k5);
	k25->adAlarendeltKcio(k3);
	k25->adAlarendeltKcio(k2);
	k15->adAlarendeltKcio(k10);
	k15->adAlarendeltKcio(k5);
	k15->adAlarendeltKcio(k3);
	k15->adAlarendeltKcio(k2);
	k10->adAlarendeltKcio(k5);
	k10->adAlarendeltKcio(k3);
	k10->adAlarendeltKcio(k2);
	k5->adAlarendeltKcio(k3);
	k5->adAlarendeltKcio(k2);
	k3->adAlarendeltKcio(k2);

	// kciok beillesztese
	(*Nuskin)->adKcio(regelt);
	(*Nuskin)->adKcio(bluediamond);
	(*Nuskin)->adKcio(diamond);
	(*Nuskin)->adKcio(emerald);
	(*Nuskin)->adKcio(ruby);
	(*Nuskin)->adKcio(lapis);
	(*Nuskin)->adKcio(gold);
	(*Nuskin)->adKcio(exec);
	(*Nuskin)->adKcio(k50);
	(*Nuskin)->adKcio(k25);
	(*Nuskin)->adKcio(k15);
	(*Nuskin)->adKcio(k10);
	(*Nuskin)->adKcio(k5);
	(*Nuskin)->adKcio(k3);
	(*Nuskin)->adKcio(k2);

	/// Szintek ///
	Szint *bd = new Szint("Blue Diamond", bluediamond, 10);
	Szint *di = new Szint("Diamond", diamond, 9);
	Szint *em = new Szint("Emerald", emerald, 8);
	Szint *ru = new Szint("Ruby", ruby, 7);
	Szint *la = new Szint("Lapis", lapis, 6);
	Szint *go = new Szint("Gold", gold, 5);
	Szint *ex = new Szint("Executive", exec, 4);
	Szint *ku2 = new Szint("Q2", q2, 3);
	Szint *ku1 = new Szint("Q1", q1, 2);
	Szint *lo = new Szint("LOI", loi, 1);
	Szint *ev = new Szint("Elsobbsegi Vasarlo", regelt, 0);

	(*Nuskin)->adSzint(bd);
	(*Nuskin)->adSzint(di);
	(*Nuskin)->adSzint(em);
	(*Nuskin)->adSzint(ru);
	(*Nuskin)->adSzint(la);
	(*Nuskin)->adSzint(go);
	(*Nuskin)->adSzint(ex);
	(*Nuskin)->adSzint(ku2);
	(*Nuskin)->adSzint(ku1);
	(*Nuskin)->adSzint(lo);
	(*Nuskin)->adSzint(ev);

	/// PontSzamito ///
	NUSpont->adMegengedettSzint(ev);
	NUSpont->adMegengedettSzint(lo);
	NUSpont->adMegengedettSzint(ku1);
	NUSpont->adMegengedettSzint(ku2); // ha vki executive ez csak akkor igaz, ha LOI, akkor csak LOIb�l kap! - egyel�re nem foglalkozunk vele

	NUSpont->adKotHavi(0, 2000);
	NUSpont->adKotHavi(1, 1000);
	NUSpont->adKotHavi(2, 1500);
	(*Nuskin)->setPontSzamito(NUSpont);
	
	/// JutalekSzamito, jutalekok, kothavik ///
	Jutalek *k2j = new Jutalek("Exec 2000", k2, 0, 6, 0.09);
		k2j->adMegengedettSzint(ev);
		k2j->adMegengedettSzint(lo);
		k2j->adMegengedettSzint(ku1);
		k2j->adMegengedettSzint(ku2);
	Jutalek *k3j = new Jutalek("Exec 3000", k3, 0, 6, 0.1);
		k3j->adMegengedettSzint(ev);
		k3j->adMegengedettSzint(lo);
		k3j->adMegengedettSzint(ku1);
		k3j->adMegengedettSzint(ku2);
	Jutalek *k5j = new Jutalek("Exec 5000", k5, 0, 6, 0.11);
		k5j->adMegengedettSzint(ev);
		k5j->adMegengedettSzint(lo);
		k5j->adMegengedettSzint(ku1);
		k5j->adMegengedettSzint(ku2);
	Jutalek *k10j = new Jutalek("Exec 10000", k10, 0, 6, 0.12);
		k10j->adMegengedettSzint(ev);
		k10j->adMegengedettSzint(lo);
		k10j->adMegengedettSzint(ku1);
		k10j->adMegengedettSzint(ku2);
	Jutalek *k15j = new Jutalek("Exec 15000", k15, 0, 6, 0.13);
		k15j->adMegengedettSzint(ev);
		k15j->adMegengedettSzint(lo);
		k15j->adMegengedettSzint(ku1);
		k15j->adMegengedettSzint(ku2);
	Jutalek *k20j = new Jutalek("Exec 25000", k25, 0, 6, 0.14);
		k20j->adMegengedettSzint(ev);
		k20j->adMegengedettSzint(lo);
		k20j->adMegengedettSzint(ku1);
		k20j->adMegengedettSzint(ku2);
	Jutalek *k25j = new Jutalek("Exec 50000", k50, 0, 6, 0.15);
		k25j->adMegengedettSzint(ev);
		k25j->adMegengedettSzint(lo);
		k25j->adMegengedettSzint(ku1);
		k25j->adMegengedettSzint(ku2);
	
	NUSjut->adJutalek(k2j);
	NUSjut->adJutalek(k3j);
	NUSjut->adJutalek(k5j);
	NUSjut->adJutalek(k10j);
	NUSjut->adJutalek(k15j);
	NUSjut->adJutalek(k20j);
	NUSjut->adJutalek(k25j);
	(*Nuskin)->setJutSzamito(NUSjut);
	
	/// Tagfelvetel, FogyFelvetel, ForgalomGeneralo ///
	*NUStagfelv = new TagFelvetel(0.8, 5, ev);
	*NUSfogyfelv = new FogyasztoFelvetel(0.8);
	*NUSforggen = new ForgalomGeneralo(10000, 3);
	(*NusEnt)->BiztositCsomagok(*NUStagfelv, *NUSfogyfelv);
	(*Nuskin)->BiztositKciok(*NUStagfelv);
}