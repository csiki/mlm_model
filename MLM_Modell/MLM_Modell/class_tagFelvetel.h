#ifndef _CLASS_TAG_FELV_
#define _CLASS_TAG_FELV_

#include "main_header.h"
#include "structs.h"

class TagFelvetel /*: public FogyasztoFelvetel*/ // örökíthető
{
	protected:
		double atlagBehivott;
		vector<Csomag*> osszCsomag; // fogyasztónál is van külön --> lehet olyan amit pl csak tagok vehetnek meg
		vector<Kvalifikacio*> osszKcio;
		unsigned nullTagletszam;
		Szint *alapszint;
	public:

		TagFelvetel(const double& atlagBehivott, const unsigned& nullTagletszam, Szint *alapszint)
			: atlagBehivott(atlagBehivott), nullTagletszam(nullTagletszam), alapszint(alapszint)
		{}

		// worker
		void NulladigFelvetel(vector<Tag*>& tagok);
		void Felvetel(vector<Tag*>& tagok, const unsigned& idoszak, const Halozat *halozat);
		void MasolCsomagok(vector<Csomag*>& csomagok);
		void MasolKciok(vector<Kvalifikacio*>& kciok);

		// getter
		const double& getAtlagBehivott() const;

		// setter
		void setAtlagBehivott(const double& set);

		// adder
		void adOsszCsomag(Csomag *cs);
};


#endif