#include "stdafx.h"
#include "main_header.h"
#include "class_vallalat.h"
#include "class_tagFelvetel.h"
#include "class_fogyasztoFelvetel.h"
#include "class_fogyaszto.h"
#include "class_tag.h"

///// workerek /////
void Vallalat::ForgalomSzamitas(vector<Tag*>& tagok, vector<Fogyaszto*>& fogyasztok)
{
	forgalom.bev = forgalom.ki = 0;

	for (unsigned t=0; t<tagok.size(); ++t)
	{
		forgalom.bev += tagok[t]->getForgalom().ki;
		forgalom.ki += tagok[t]->getForgalom().bev;
	}

	for (unsigned f=0; f<fogyasztok.size(); ++f)
	{
		forgalom.bev += fogyasztok[f]->getForgalom().ki;
		forgalom.ki += fogyasztok[f]->getForgalom().bev;
	}

	osszbev += forgalom.bev + egyebForgalom.bev;
	osszki += forgalom.ki + egyebForgalom.ki;
}

void Vallalat::BiztositCsomagok(TagFelvetel *tagfelv, FogyasztoFelvetel *fogyfelv)
{
	tagfelv->MasolCsomagok(csomagok);
	fogyfelv->MasolCsomagok(csomagok);
}

///// adderek /////
void Vallalat::adCsomag(Csomag *minta)
{
	if (minta != NULL) csomagok.push_back(minta);
	else throw "Nem letezo csomag!";
}

///// getterek /////
const string& Vallalat::getNev() const
{
	return nev;
}

const Forgalom& Vallalat::getForgalom() const
{
	return forgalom;
}

const long double& Vallalat::getOsszKi() const
{
	return osszbev;
}

const long double& Vallalat::getOsszBev() const
{
	return osszki;
}