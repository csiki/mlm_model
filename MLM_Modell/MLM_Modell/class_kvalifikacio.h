#ifndef _CLASS_KVALIFIKACIO_
#define _CLASS_KVALIFIKACIO_

#include "main_header.h"

class Kvalifikacio // �r�k�thet�
{
	protected:
		unsigned kozvetlenekSzama;
		vector<Szint*> megengedettKozvSzintek; // ha 0 m�ret�, nem sz�m�t milyen szint�ek
		double long osszBev;
		double elozoBev;
		double sajatPont;
		double pont;
		double osszPont;
		Csomag *csomag;
		vector<Kvalifikacio*> alarendeltKciok; // amik azt�n nem fognak beletartozni a kciok k�z�, ha ez a kcio teljes�l
		bool statikus; // ha m�r egyszer megszerezt�k, nem lehet levedleni
	public:
		bool athelyezni;

		Kvalifikacio(const bool& statikus = true, const unsigned& kozvetlenekSzama = 0, const double& sajatPont = 0, const double& pont = 0, const double& osszPont = 0, const unsigned& osszBev = 0, const unsigned& elozoBev = 0, Csomag *csomag = NULL) : 
			statikus(statikus),
			kozvetlenekSzama(kozvetlenekSzama),
			sajatPont(sajatPont),
			pont(pont),
			osszPont(osszPont),
			osszBev(osszBev),
			elozoBev(elozoBev),
			csomag(csomag)
		{
			athelyezni = false;
		}

		// workerek
		bool Kvalifalt(Tag *tag);

		// getterek
		const unsigned& getKozvSzama() const;
		const unsigned long& getOsszBev() const;
		const unsigned& getElozoBev() const;
		Csomag *getCsomag() const;
		Kvalifikacio *getAlarendeltKcio(const unsigned& index, bool *letezik) const;
		const bool& Statikus() const;
		Szint *getMegengKozvSzint(const unsigned& index, bool *letezik) const;
		const double& getPont() const;

		// setterek
		void setKozvSzama(const unsigned& set);
		void setOsszBev(const unsigned long& set);
		void setElozoBev(const unsigned& set);
		void setCsomag(Csomag *set);
		void setAlarendeltKcio(Kvalifikacio *minta, const unsigned& index);
		void setStatikus(const bool& set);
		void setMegengKozvSzint(Szint *minta, const unsigned& index);

		// adderek
		void adAlarendeltKcio(Kvalifikacio *minta);
		void adMegengKozvSzint(Szint *minta);
};

#endif