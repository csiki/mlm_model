#ifndef _CLASS_FORG_GEN_
#define _CLASS_FORG_GEN_

#include "main_header.h"

class ForgalomGeneralo // örökíthető
{
	protected:
		double maxKolt;
		unsigned dbTermekProbal;
	public:

		ForgalomGeneralo(const double& maxKolt, const unsigned& dbTermekProbal)
			: maxKolt(maxKolt), dbTermekProbal(dbTermekProbal)
		{}

		// workerek
		void General(vector<Tag*>& tagok, vector<Fogyaszto*>& fogyasztok);

		// getter
		const unsigned& getMaxKolt() const;
		const unsigned& getTermekProbal() const;

		// setter
		void setMaxKolt(const unsigned& set);
		void setTermekProbal(const unsigned& set);
};

#endif