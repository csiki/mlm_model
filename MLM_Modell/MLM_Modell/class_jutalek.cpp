#include "stdafx.h"
#include "main_header.h"
#include "class_jutalek.h"
#include "class_tag.h"
#include "class_szint.h"

void WaitForKey();

///// workerek /////
const double& Jutalek::Kiszamol(Tag *tag)
{
	double jut = 0;
	unsigned tol = melyseg.tol;

	if (melyseg.tol == 0)
	{
		tol = 1;
		jut += tag->getOsszKiadas() * szazalek;
	}
	
	map<Szint*,double>::iterator talalat;
	map<Szint*,double>::iterator it;
	for (unsigned g = tol; g <= melyseg.ig && g < tag->genKiadas.size(); ++g)
	{
		for (it=tag->genKiadas[g].begin(); it!=tag->genKiadas[g].end(); ++it)
		{
			if (find(megengedettSzintek.begin(), megengedettSzintek.end(), it->first) != megengedettSzintek.end())
				jut += it->second * szazalek;
		}
	}
	
	return jut;
}

///// getterek /////
const Melyseg& Jutalek::getMelyseg() const
{
	return melyseg;
}

Kvalifikacio* Jutalek::getKcio() const
{
	return kcio;
}

const bool& Jutalek::getVegtelenMely() const
{
	return vegtelenMely;
}

///// adderek /////
void Jutalek::adMegengedettSzint(Szint *minta)
{
	megengedettSzintek.push_back(minta);
}