#ifndef _CLASS_SZIMULACIO_
#define _CLASS_SZIMULACIO_

#include "main_header.h"
#include "class_vallalat.h"
#include "class_jutalek.h"
#include "class_szint.h"
#include "class_fogyaszto.h"
#include "class_halozat.h"
#include "class_tag.h"
#include "class_fogyasztoFelvetel.h"
#include "class_tagFelvetel.h"
#include "class_forgGen.h"

class Szimulacio
{
	private:
		string nev;
		Halozat *halozat;
		Vallalat *vallalat;
		unsigned idoszak;
		vector<Tag*> tagok;
		vector<Fogyaszto*> fogyasztok;
		vector<Tag*> levelek;
		bool halozatLementve; // többször fölöslegesen ne mentse le
		ForgalomGeneralo *forgGen;
		TagFelvetel *tagfelv;
		FogyasztoFelvetel *fogyfelv;

		// belső workerek
		bool Betolt(const char *allapotFajlNev, const char *halozatFajlNev, const char *vallalatFajlNev);

	public:
		const static unsigned processzorSzam = 8;

		Szimulacio(Halozat *halozat, Vallalat *vallalat, TagFelvetel *tagfelv, FogyasztoFelvetel *fogyfelv, ForgalomGeneralo *forgGen, const char* nev, const unsigned& idoszak = 0) : 
			halozat(halozat),
			vallalat(vallalat),
			tagfelv(tagfelv),
			fogyfelv(fogyfelv),
			forgGen(forgGen),
			nev(nev),
			idoszak(idoszak)
			{
				halozatLementve = false;
			}
		~Szimulacio();

		// workerek
		void Start(unsigned *aktualisIdosz, const unsigned& ido = 1, const bool& ment = false, const bool& kiirat = false, ostream& os = cout);
		void Futtat();
		void Kiirat(ostream& os = cout);
		virtual void Nullazas();

		// perzisztencia
		void MentHalozat();
		void MentAllapot();
		void MentVallalat();
		bool Betolt(const char *mappanev); // halozatLementve = true

		// getterek
		const Tag& getTag(const unsigned& index, bool *letezik) const;
		const Fogyaszto& getFogyaszto(const unsigned& index, bool *letezik) const;
		const unsigned& getIdoszak() const;
		const ForgalomGeneralo& getForgGen() const;

		// setterek
		void setFogyaszto(const Fogyaszto& minta, const unsigned& id);
		void setTag(const Tag& minta, const unsigned& id);
		void setForgGen(const ForgalomGeneralo& minta);

		// adderek
		void adFogyaszto(const unsigned& db, const unsigned& spid);
		void adFogyaszto(const unsigned& db, const Fogyasztas& minta);
		void adFogyaszto(const unsigned& db, const Fogyasztas& minta, const unsigned& spid);
		void adTag(const unsigned& db, const unsigned& spid);
		void adTag(const unsigned& db, const Tag& minta);
		void adTag(const unsigned& db, const Tag& minta, const unsigned& spid);
};

#endif