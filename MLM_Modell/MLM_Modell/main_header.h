#ifndef _MAIN_HEADER_
#define _MAIN_HEADER_

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <deque>
#include <string>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <numeric>
#include <iomanip>

using namespace std;

enum KompenzaciosTerv {BREAKAWAY, BINARIS, MATRIX, UNILEVEL, EGYEB};
enum LabAlapuElv {NEM_LABALAPU, ROVIDEBB_LAB}; // b�v�thet�

struct Melyseg;
struct Fogyyasztas;
struct Forgalom;
struct Csomag;

class Kvalifikacio;
class Jutalek;
class Szint;
class Vallalat;
class Fogyaszto;
class Tag;
class Halozat;
class Szimulacio;
class JutalekSzamito;
class PontSzamito;
class ForgalomGeneralo;
class TagFelvetel;
class FogyasztoFelvetel;

enum LabAlapuElv;

#endif

