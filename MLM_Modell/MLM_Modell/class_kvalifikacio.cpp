#include "stdafx.h"
#include "main_header.h"
#include "class_kvalifikacio.h"
#include "class_tag.h"
#include "structs.h"
#include "class_szint.h"
#include <algorithm>

///// workerek /////
bool Kvalifikacio::Kvalifalt(Tag *tag)
{
	if  (
		!athelyezni &&
		kozvetlenekSzama <= tag->getKozvetlenDb() &&
		osszBev <= tag->getOsszForgalom().bev &&
		elozoBev <= tag->getForgalom().bev &&
		sajatPont <= tag->getSajatPont() &&
		pont <= tag->getPont() &&
		osszPont <= tag->getOsszPont()
		)
	{
		// csomag
		bool csomagMegvan = true;
		if (csomag != NULL)
		{
			csomagMegvan = false;
			vector<Fogyasztas>::iterator it;
			vector<Fogyasztas>& fogy = tag->getFogyasztas();
			for (it=fogy.begin(); it<fogy.end() && !csomagMegvan; ++it)
				if (it->csomag == csomag) csomagMegvan = true;
		}
		
		// kozvetlen
		bool kozvetlenekMegvannak = true;
		if (megengedettKozvSzintek.size() > 0)
		{
			kozvetlenekMegvannak = false;
			unsigned kozvDb = 0;
			vector<Szint*>::iterator szIt;
			vector<Tag*>::iterator tIt;
			vector<Tag*>& kozvetlenek = tag->accessKozvetlenek();

			for (szIt=megengedettKozvSzintek.begin(); szIt<megengedettKozvSzintek.end() && !kozvetlenekMegvannak; ++szIt)
			{
				for (tIt=kozvetlenek.begin(); tIt<kozvetlenek.end() && !kozvetlenekMegvannak; ++tIt)
				{
					if ((*tIt)->getSzint() == (*szIt)) ++kozvDb;
					if (kozvDb >= kozvetlenekSzama) kozvetlenekMegvannak = true;
				}
			}
		}

		if (kozvetlenekMegvannak && csomagMegvan)
		{
			vector<Kvalifikacio*>& kciok = tag->accessKciok();
			vector<Kvalifikacio*>& nemKvalifalt = tag->accessNemKvalifalt();
			vector<Kvalifikacio*>::iterator begin = nemKvalifalt.begin();
			vector<Kvalifikacio*>::iterator it;
			vector<Kvalifikacio*>::iterator talalat;
			for (it=alarendeltKciok.begin(); it<alarendeltKciok.end(); ++it)
			{
				talalat = std::find(kciok.begin(), kciok.end(), *it);
				if (talalat != kciok.end())
					(*talalat)->athelyezni = true;
			}
			return true;
		}
	}
	return false;
}

///// getterek /////
const bool& Kvalifikacio::Statikus() const
{
	return statikus;
}

const unsigned& Kvalifikacio::getElozoBev() const
{
	return elozoBev;
}

const double& Kvalifikacio::getPont() const
{
	return pont;
}

///// adderek /////
void Kvalifikacio::adAlarendeltKcio(Kvalifikacio *minta)
{
	if (minta != NULL) alarendeltKciok.push_back(minta);
	else throw "Nem letezo kvalifikacio!";
}

void Kvalifikacio::adMegengKozvSzint(Szint *minta)
{
	if (minta != NULL) megengedettKozvSzintek.push_back(minta);
	else throw "Nem letezo szint!";
}